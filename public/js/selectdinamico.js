//  INICION Seleccionar el departamentopor medio del ID del PAÍS
$(function() {
    //alert('Aqui toy'); //es para validar que realmente se integro bien el archivo
    $('#country_id').on('change',
        selectDepartments);
});

function selectDepartments() {
    var country_id = $(this).val();
    $.get('departments/' + country_id, function(data) {
        $("#department_id").empty();
        var html_select = '<option  value="">Seleccione un Departamento</option>';
        for (var p in data) {
            html_select += '<option  value="' + data[p].id + '">' + data[p].name + '</option>';
        }
        $('#department_id').html(html_select);
    })
}

// FIN Seleccionar el departamentopor medio del ID del PAÍS

//  INICION Seleccionar la CIUDAD medio del ID del DEPARTAMENTO
$(function() {
    $('#department_id').on('change',
        selectCitys);
});

function selectCitys() {
    var department_id = $(this).val();
    $.get('citys/' + department_id, function(data) {
        $("#city_id").empty();
        var html_select = '<option  value="">Seleccione una ciudad</option>';
        for (var p in data) {
            html_select += '<option  value="' + data[p].id + '">' + data[p].name + '</option>';
        }
        $('#city_id').html(html_select);
    })
}

// FIN Seleccionar la CIUDAD medio del ID del DEPARTAMENTO