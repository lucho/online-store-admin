$.extend($.validator.messages, {
    required: "Este campo es requerido",
    maxlength: $.validator.format("Ingrese un máximo de {0} caracteres."),
    minlength: $.validator.format("Ingrese al menos {0} caracteres."),
    rangelength: $.validator.format("Ingrese al menos {0} y un máximo de {1} caracteres."),
    email: "Por favor ingrese un correo valido",
    url: "Por favor, introduzca una URL válida.",
    date: "Por favor, introduzca una fecha válida.",
    number: "Por favor ingrese un número.",
    digits: "Por favor solo ingrese dígitos.",
    equalTo: "Por favor repita el mismo valor.",
    range: $.validator.format("Ingrese un valor entre {0} y {1}."),
    max: $.validator.format("Ingrese un valor menor o igual a {0}."),
    min: $.validator.format("Ingrese un valor mayor o igual a {0}."),
    creditcard: "Ingrese un número de tarjeta de crédito válido."
});

$("#id").validate({
    rules: {
        name: {
            required: true,
            email: true
        }
    },
    submitHandler: function(form) {
        form.submit();
    }
});