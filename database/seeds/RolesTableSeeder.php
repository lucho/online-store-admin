<?php

use Illuminate\Database\Seeder;
use App\Models\Security\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'id' => 1,
            'name' => 'Administrador App',
        ]);
        Role::create([
            'id' => 2,
            'name' => 'Administrador',
        ]);
        Role::create([
            'id' => 3,
            'name' => 'Analista',
        ]);
        Role::create([
            'id' => 4,
            'name' => 'Usuario',
        ]);
    }
}
