<?php

use Illuminate\Database\Seeder;
use App\Models\Parameters\Genre;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Genre::create([
            'id' => 1,
            'name' => 'Masculino'
        ]);
        Genre::create([
            'id' => 2,
            'name' => 'Femenino'
        ]);
    }
}
