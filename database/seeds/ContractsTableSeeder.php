<?php

use Illuminate\Database\Seeder;
use App\Models\Mediators\Contract;

class ContractsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contract::create([
            'id' => 1,
            'name' => 'servicio',
            'date' => '2020-01-01',
            'contracts_type_id' => '1',
            'description'=>' Descripcion del archivo',
            'company_id' => '1'
        ]);
    }
}
