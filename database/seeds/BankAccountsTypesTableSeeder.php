<?php

use Illuminate\Database\Seeder;
use App\Models\Parameters\BankAccountsTypes;

class BankAccountsTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BankAccountsTypes::create([
            'id' => 1,
            'name' => 'Cuenta corriente'
        ]);
        BankAccountsTypes::create([
            'id' => 2,
            'name' => 'Cuenta de ahorro'
        ]);
        BankAccountsTypes::create([
            'id' => 3,
            'name' => 'Cuenta de nómina'
        ]);
        BankAccountsTypes::create([
            'id' => 4,
            'name' => 'Cuenta de valor'
        ]);
        BankAccountsTypes::create([
            'id' => 5,
            'name' => 'Cuenta de negocio'
        ]);
        BankAccountsTypes::create([
            'id' => 6,
            'name' => 'Cuenta de empresa'
        ]);
    }
}
