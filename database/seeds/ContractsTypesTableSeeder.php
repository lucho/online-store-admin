<?php

use Illuminate\Database\Seeder;
use App\Models\Parameters\ContractsType;

class ContractsTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ContractsType::create([
            'id' => 1,
            'name' => 'Contrato de servicio',
        ]);
        ContractsType::create([
            'id' => 2,
            'name' => 'Contrato de Arrendamiento',
        ]);
        ContractsType::create([
            'id' => 3,
            'name' => 'Contrato de Trabajo',
        ]);
        ContractsType::create([
            'id' => 4,
            'name' => 'Contrato de venta',
        ]);
        ContractsType::create([
            'id' => 5,
            'name' => 'Acuerdo de licencia',
        ]);
    }
}
