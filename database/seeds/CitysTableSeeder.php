<?php

use Illuminate\Database\Seeder;
use App\Models\Parameters\City;

class CitysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        City::create([
            'id' => 1,
            'name' => 'Bogotá D.C',
            'department_id' => '1'
        ]);
    }
}
