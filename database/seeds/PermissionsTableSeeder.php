<?php

use Illuminate\Database\Seeder;
use App\Models\Security\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
            'id' => 1,
            'name' => 'view',
        ]);
        Permission::create([
            'id' => 2,
            'name' => 'list',
        ]);
        Permission::create([
            'id' => 3,
            'name' => 'create',
        ]);
        Permission::create([
            'id' => 4,
            'name' => 'update',
        ]);
        Permission::create([
            'id' => 5,
            'name' => 'delete',
        ]);
    }
}
