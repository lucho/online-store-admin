<?php

use Illuminate\Database\Seeder;
use App\Models\Parameters\IdentificationsType;

class IdentificationsTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        IdentificationsType::create([
            'id' => 1,
            'name' => 'NIT',
            'enabled'=> '1'
        ]);
        IdentificationsType::create([
            'id' => 2,
            'name' => 'CC',
            'enabled' => '1'
        ]);
    }
}
