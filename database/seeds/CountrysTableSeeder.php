<?php

use Illuminate\Database\Seeder;
use App\Models\Parameters\Country;

class CountrysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::create([
            'id' => 1,
            'name' => 'Colombia',
        ]);
    }
}
