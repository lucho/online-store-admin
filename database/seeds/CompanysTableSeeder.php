<?php

use Illuminate\Database\Seeder;
use App\Models\Mediators\Company;

class CompanysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::create([
            'id' => 1,
            'name' => 'LEGA',
            'identifications_type_id'=> '1',
            'document_number' => '00000000',
            'email' => 'Admin@lega.com',
            'phone' => '3204781884',
            'direction' => 'Calle 150 # 50',
            'city_id' => '1'
          ]);
    }
}
