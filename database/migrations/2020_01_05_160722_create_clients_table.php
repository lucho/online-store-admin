<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('names');
            $table->string('surnames');
            $table->unsignedBigInteger('identifications_type_id');
            $table->string('document_number')->unique();
            $table->string('email');
            $table->string('phone');
            $table->string('direction');
            $table->date('birth_date_id');
            $table->string('neighborhood');
            $table->string('delivery_direction');
            $table->string('bulletin');
            $table->unsignedBigInteger('city_id');
            $table->unsignedBigInteger('genre_id');
            $table->boolean('enabled')->default(true);
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::table('clients', function (Blueprint $table) {
            $table->foreign('identifications_type_id')->references('id')->on('identifications_types')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('citys')->onDelete('cascade');
            $table->foreign('genre_id')->references('id')->on('genres')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
