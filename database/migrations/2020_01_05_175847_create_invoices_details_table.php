<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_name');
            $table->string('reference');
            $table->text('product_description');
            $table->decimal('product_cost_price', 12, 4);
            $table->decimal('product_cost_iva', 12, 4);
            $table->decimal('product_price_sale', 12, 4);
            $table->decimal('discount', 12, 4);
            $table->string('amount');
            $table->string('promotion');
            $table->text('guarantee');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('supplier_id');
            $table->unsignedBigInteger('invoice_id');       
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices_details');
    }
}
