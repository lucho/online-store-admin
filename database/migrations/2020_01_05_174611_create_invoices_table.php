<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('date');
            $table->string('identification_number');
            $table->string('client_names');
            $table->string('client_surnames');
            $table->string('client_phone');
            $table->string('client_email');
            $table->string('contact_client_names');
            $table->string('contact_client_surnames');
            $table->string('contact_client_phone');
            $table->string('contact_client_email');
            $table->decimal('value', 12, 4);
            $table->decimal('value_iva', 12, 4);
            $table->decimal('discount', 12, 4);
            $table->decimal('total_value', 12, 4);
            $table->string('payment_method');
            $table->text('direction');
            $table->text('delivery_direction');
            $table->string('neighborhood');
            $table->unsignedBigInteger('sale_id');
            $table->unsignedBigInteger('identifications_type_id');
            $table->unsignedBigInteger('city_id');       
            $table->boolean('enabled')->default(true);
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::table('invoices', function (Blueprint $table) {
            $table->foreign('sale_id')->references('id')->on('sales');
            $table->foreign('identifications_type_id')->references('id')->on('identifications_types');
            $table->foreign('city_id')->references('id')->on('citys');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
