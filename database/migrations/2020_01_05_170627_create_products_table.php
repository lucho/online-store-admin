<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('reference');
            $table->text('description');
            $table->text('barcode');
            $table->string('alias');
            $table->decimal('cost_price', 12, 4);
            $table->decimal('cost_price_iva', 12, 4);
            $table->decimal('sale_price', 12, 4);
            $table->decimal('sale_price_iva', 12, 4);
            $table->decimal('cost_freight', 12, 4);
            $table->string('utility_percentage');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('long');
            $table->string('width');
            $table->string('high');
            $table->string('weight');
            $table->unsignedBigInteger('brand_id');
            $table->unsignedBigInteger('supplier_id');
            $table->unsignedBigInteger('conveyor_id');
            $table->unsignedBigInteger('units_measurement_id');
            $table->unsignedBigInteger('guarantee_id')->nullable();
            $table->unsignedBigInteger('categorie_id');
            $table->unsignedBigInteger('currency_type_id')->nullable();
            $table->unsignedBigInteger('inventary_id');
            $table->unsignedBigInteger('company_id');
            $table->boolean('enabled')->default(true);
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::table('products', function (Blueprint $table) {
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
            $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade');
            $table->foreign('conveyor_id')->references('id')->on('conveyors')->onDelete('cascade');
            $table->foreign('units_measurement_id')->references('id')->on('units_measurements')->onDelete('cascade');
            $table->foreign('guarantee_id')->references('id')->on('guarantees')->onDelete('cascade');
            $table->foreign('categorie_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('currency_type_id')->references('id')->on('currency_types')->onDelete('cascade');
            $table->foreign('inventary_id')->references('id')->on('inventarys')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companys')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
