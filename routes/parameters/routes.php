<?php

// Ciudades
Route::apiResource('countrys', 'Parameters\CountryController', ['only' => [
    'index',
    'create',
    'store',
    'show',
    'edit',
    'update',
    'destroy',
]]);

// Departamentos
Route::get('citys/departments/{country_id}', 'Parameters\DepartmentController@getDepartmentsByCountry');
Route::get('citys/{old_country_id}/departments/{country_id}', 'Parameters\DepartmentController@getDepartmentsByCountry');

Route::apiResource('departments', 'Parameters\DepartmentController', ['only' => [
    'index',
    'create',
    'store',
    'show',
    'edit',
    'update',
    'destroy',
]]);
//Ciudades
Route::apiResource('citys', 'Parameters\CityController', ['only' => [
    'index',
    'create',
    'store',
    'show',
    'edit',
    'update',
    'destroy',
]]);
//Monedas
Route::apiResource('currencyttypes', 'Parameters\CurrencyTypeController', ['only' => [
    'index',
    'create',
    'store',
    'show',
    'edit',
    'update',
    'destroy',
]]);
//TRM
Route::apiResource('trms', 'Parameters\TrmController', ['only' => [
    'index',
    'create',
    'store',
    'show',
    'edit',
    'update',
    'destroy',
]]);
// Bancos
Route::apiResource('banks', 'Parameters\BankController', ['only' => [
    'index',
    'create',
    'store',
    'show',
    'edit',
    'update',
    'destroy',
]]);

// Cuentas Bancarias
Route::apiResource('bankAccounts', 'Parameters\BankAccountController', ['only' => [
    'index',
    'create',
    'store',
    'show',
    'edit',
    'update',
    'destroy',
]]);
//tipo de Pago
Route::apiResource('paymenttypes', 'Parameters\PaymentTypeController', ['only' => [
    'index',
    'create',
    'store',
    'show',
    'edit',
    'update',
    'destroy',
]]);