<?php

// Usuarios

Route::apiResource('users', 'Security\UserController', ['only' => [
    'index',
    'create',
    'store',
    'show',
    'edit',
    'update',
    'destroy',
]]);
