<?php

// Empresa
Route::get('companys/departments/{country_id}', 'Parameters\DepartmentController@getDepartmentsByCountry');
Route::get('companys/{old_country_id}/departments/{country_id}', 'Parameters\DepartmentController@getDepartmentsByCountry');
Route::get('companys/citys/{department_id}', 'Parameters\CityController@getCitysByDepartment');
Route::get('companys/{old_department_id}/citys/{department_id}', 'Parameters\CityController@getCitysByDepartment');
Route::apiResource('companys', 'Mediators\CompanyController', ['only' => [
    'index',
    'create',
    'store',
    'show',
    'edit',
    'update',
    'destroy',
]]);
//Trasportadoras
Route::apiResource('Conveyors', 'Mediators\ConveyorController', ['only' => [
    'index',
    'create',
    'store',
    'show',
    'edit',
    'update',
    'destroy',
    ]]); 