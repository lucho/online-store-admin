@extends('layouts.app')

@section('css')
<link href="{{asset('template/vendor/dropify/dist/css/dropify.min.css')}}" rel="stylesheet">
<link href="{{asset('template/vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
@endsection

@section('content')
{{-- Titulo --}}
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Terceros</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Transportadora</a></li>
                <li class="breadcrumb-item active"><a href="javascript:void(0)">Actualizar</a></li>
            </ol>
        </div>
    </div>
</div>
{{-- Titulo --}}
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Actualización de una Transportadora</h4>
                <h6 class="card-subtitle">Los campos que tienen el * son campos OBLIGATORIOS</h6>
                {!!Form::open(array('method'=>'PATCH','route'=>['Conveyors.update',$conveyor->id],'files'=>'true'))!!}
                {{Form::token()}}
                <div class="form-row">
                <div class="form-group col-md-4 mb-6">
                    <h5>Nombre<span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="name" class="form-control" required data-validation-required-message="Campo requerido" value="{{$conveyor->name}}"> 
                        </div>
                 </div>
                 <div class="form-group col-md-6 mb-6">
                     <h5>Tipo de Documento <span class="text-danger">*</span></h5>
                        <div class="controls">
                           <select name="identifications_type_id" id="identifications_type_id" required class="form-control">
                             <option value="">Seleccione un tipo de identificación</option>
                                @foreach ($identificationsTypes as $item)
                                    @if($item->id == $conveyor->identifications_type_id)
                                    <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                        @else
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                 </div>
                 <div class="form-group col-md-6 mb-6">
                     <h5>Número de documento<span class="text-danger">*</span></h5>
                        <div class="controls">
                          <input type="number" name="document_number" class="form-control" required value="{{$conveyor->document_number}}"> </div>
                 </div>
                 <div class="form-group col-md-6 mb-12">
                     <h5>Correo<span class="text-danger">*</span></h5>
                         <div class="controls">
                           <input type="email" name="email" class="form-control" required value="{{$conveyor->email}}"> </div>
                         </div>
                 <div class="form-group col-md-6 mb-6">
                      <h5>Número Telefono<span class="text-danger">*</span></h5>
                         <div class="controls">
                            <input type="number" name="phone" class="form-control" required value="{{$conveyor->phone}}"> </div>
                         </div>
                 <div class="form-group col-md-6 mb-6">
                      <h5>Dirección<span class="text-danger">*</span></h5>
                         <div class="controls">
                           <input type="text" name="direction" class="form-control" required value="{{$conveyor->direction}}"> </div>
                 </div>    
                 
               </div>
                <div class="text-xs-center">
                    <button type="submit" class="btn btn-info">Actualizar</button>
                </div>
            </div>
        </div>
        {!!Form::close()!!}
    </div>
</div>




</div>
</div>
@endsection

@section('js')
<script src="{{asset('template/js/pages/jasny-bootstrap.js')}}"></script>
<script src="{{asset('template/js/pages/validation.js')}}"></script>
<!-- jQuery file upload -->
<script src="{{asset('template/vendor/dropify/dist/js/dropify.min.js')}}"></script>
<!-- /jQuery file upload -->
<!-- Validator-->
<script>
    ! function(window, document, $) {
        "use strict";
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }(window, document, jQuery);
</script>
<!--/ Validator-->
<!--- Upload--->
<script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
</script>
<!--- /Upload--->

<!-- DataPickerts -->
<script src="{{asset('template/vendor/moment/moment.js')}}"></script>
<script src="{{asset('template/vendor/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
<!-- Date range Plugin JavaScript -->

<!-- /DataPickerts -->

<script>
    // MAterial Date picker    
    $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    // Clock pickers
    $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });
    $('.clockpicker').clockpicker({
        donetext: 'Done',
    }).find('input').change(function() {
        console.log(this.value);
    });
    
    if (/mobile/i.test(navigator.userAgent)) {
        $('input').prop('readOnly', true);
    }
   
    
    </script>

@endsection