@extends('layouts.app')
@section('css')
<link href="{{asset('template/vendor/bootstrap-table/dist/bootstrap-table.min.css')}}" rel="stylesheet">
@endsection
@section('content')
{{-- Titulo --}}
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Lista de Transportadoras</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Terceros</a></li>
                <li class="breadcrumb-item active">Transportadoras</li>
            </ol>
            <form action="{{ route('Conveyors.create') }}">
                <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-info m-l-40 " href="{{ route('Conveyors.create') }}"><i class="ti-plus"></i> Crear Transportadoras</button>
            </form>
        </div>
    </div>
</div>
{{-- Titulo --}}

@include('alerts.success_and_fail')
@include('mediators.company.search')

<div class="row">
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive ">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Tipo documento</th>
                                <th>Número documento</th>
                                <th>email</th>
                                <th>Telefono</th>
                                <th>Dirección</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($conveyors as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->name }}</td>
                                <td> {{ $item->identificationsType->name }}</td>
                                <td> {{ $item->document_number }}</td>
                                <td> {{ $item->email }}</td>
                                <td> {{ $item->phone }}</td>
                                <td> {{ $item->direction }}</td>
                                <td>@if($item->enabled == 1) ACTIVO @else INACTIVO @endif</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="ti-settings"></i>
                                        </button>
                                         <div class="dropdown-menu animated flipInX">
                                            <a class="dropdown-item" href="{{ route('Conveyors.edit', $item) }}">Actualizar</a>
                                            <a href="" data-target="#modal-delete-{{$item->id}}"
                                                data-toggle="modal" class="dropdown-item">@if($item->enabled == 1) Inactivar  @else Activar  @endif</a>
                                        </div> 
                                    </div>
                                </td>
                            </tr>
                            @include('mediators\company.Conveyors.modal')
                            @endforeach
                        </tbody>
                    </table>
                    {{$conveyors->render()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('template/vendor/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
<script src="{{asset('template/vendor/bootstrap-table/dist/bootstrap-table.min.js')}}"></script>
<script src="{{asset('template/vendor/peity/jquery.peity.min.js')}}"></script>
<script src="{{asset('template/vendor/peity/jquery.peity.init.js')}}"></script>

@endsection