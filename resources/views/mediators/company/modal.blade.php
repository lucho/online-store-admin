<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$item->id}}">
	{{Form::Open(array('action'=>array('Mediators\CompanyController@destroy',$item->id),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
                <h4 class="modal-title">@if($item->enabled == 1) Inactivar  @else Activar @endif Empresa</h4>
			</div>
			<div class="modal-body">
				<p>Confirme si desea @if($item->enabled == 1) Inactivar  @else Activar @endif a {{$item->name}}</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>