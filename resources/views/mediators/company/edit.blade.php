@extends('layouts.app')

@section('css')
{{-- wizard --}}
<link href="{{asset('template/vendor/wizard/steps.css')}}" rel="stylesheet">
{{-- /wizard --}}
{{-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css"> --}}
{{-- <link href="{{asset('mbd/css/mdb.min.css')}}" rel="stylesheet"> --}}
<link href="{{asset('mbd/css/style.css')}}" rel="stylesheet">
<link href="{{asset('css/header-modal-color.css')}}" rel="stylesheet">
<link href="{{asset('template/vendor/dropify/dist/css/dropify.min.css')}}" rel="stylesheet">

@endsection

@section('content')
{{-- Titulo --}}
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Terceros</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Empresa</a></li>
                <li class="breadcrumb-item active"><a href="javascript:void(0)">Actualizar</a></li>
            </ol>
        </div>
    </div>
</div>
{{-- Titulo --}}
{{-- Contenido --}}
<div class="row" id="validation">
    <div class="col-12">
        <div class="card wizard-content">
            <div class="card-body">
                <h4 class="card-title">Actualización de una empresa</h4>
                <h6 class="card-subtitle">Los campos que tienen el * son campos OBLIGATORIOS</h6>
                    {!!Form::open(array('method'=>'PATCH','route'=>['companys.update',$company->id],'files'=>'true','class'=> 'validation-wizard wizard-circle'))!!}
                    {{Form::token()}}
                    <!-- Step 1 -->
                    <h6>Datos básicos</h6>
                    <section>
                        <div class="row">
                            <div class="form-group col-md-6 mb-6">
                                <h5>Nombre<span class="text-danger">*</span></h5>
                                <div class="controls">
                                <input type="text" name="name" class="form-control" required value="{{$company->name}}"> </div>
                            </div>
                            <div class="form-group col-md-6 mb-6">
                                <h5>Tipo de Documento <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <select name="identifications_type_id" id="identifications_type_id" required class="form-control">
                                        <option value="">Seleccione un tipo de identificación</option>
                                        @foreach ($identificationsTypes as $item)
                                        @if($item->id == $company->identificationsType->id)
                                      <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                     @else
                                       <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-6 mb-6">
                                <h5>Número de documento<span class="text-danger">*</span></h5>
                                <div class="controls">
                                <input type="number" name="document_number" class="form-control" required value="{{$company->document_number}}"> </div>
                            </div>
                            <div class="form-group col-md-6 mb-12">
                                <h5>Correo<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="email" name="email" class="form-control" required value="{{$company->email}}"> </div>
                            </div>
                            <div class="form-group col-md-6 mb-6">
                                <h5>Número Telefono<span class="text-danger">*</span></h5>
                                <div class="controls">
                                <input type="number" name="phone" class="form-control" required value="{{$company->phone}}"> </div>
                            </div>
                            <div class="form-group col-md-6 mb-6">
                                <h5>Dirección<span class="text-danger">*</span></h5>
                                <div class="controls">
                                <input type="text" name="direction" class="form-control" required value="{{$company->direction}}"> </div>
                            </div>    
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4 mb-6">
                                <h5>País <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <select name="country_id" id="country_id" required class="form-control" >
                                        <option value="">Seleccione un país</option>
                                        @foreach ($countrys as $item)
                                        @if($item->id == $company->city->department->country->id)
                                            <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                         @else
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-4 mb-6">
                                <h5>Departamento <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <select name="department_id" id="department_id" required class="form-control" >
                                        <option value="">Seleccione un Departamento</option>  
                                        @foreach ($departments as $item)
                                        @if($item->id == $company->city->department->id)
                                        <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-4 mb-6">
                                <h5>Ciudad <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <select name="city_id" id="city_id" required class="form-control" >
                                        <option value="">Seleccione una ciudad</option>
                                        @foreach ($citys as $item)
                                        @if($item->id == $company->city->id)
                                        <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- Step 2 -->
                    <h6>Contacto</h6>
                    <section>
                        <div class="row">
                            <div class="form-group col-md-12 mb-6">
                                <h5>Nombre<span class="text-danger">*</span></h5>
                                <div class="controls">
                                <input type="text" name="contactName" class="form-control" required value="{{$company->contacts[0]->name}}"> </div>
                            </div>
                            <div class="form-group col-md-6 mb-12">
                                <h5>Correo<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="email" name="contactEmail" class="form-control" required value="{{$company->contacts[0]->email}}"> </div>
                            </div>
                            <div class="form-group col-md-6 mb-6">
                                <h5>Número Telefono<span class="text-danger">*</span></h5>
                                <div class="controls">
                                <input type="number" name="contactPhone" class="form-control" required value="{{$company->contacts[0]->phone}}"> </div>
                            </div>
                        </div>
                    </section>
                    <!-- Step 3 -->
                    <h6>Contrato</h6>
                    <section>
                        <div class="row">
                            <div class="form-group col-md-6 mb-6">
                                <h5>Tipo de contrato <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <select name="contracts_type_id" id="contracts_type_id" required class="form-control" >
                                        <option value="">Seleccione un tipo de contrato</option>
                                        @foreach ($contractsTypes as $item)
                                        @if($item->id == $company->contracts[0]->contracts_type_id)
                                        <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                          @else
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-6 mb-6">
                                <h5>Nombre<span class="text-danger">*</span></h5>
                                <div class="controls">
                                <input type="text" name="contractName" class="form-control" required value="{{$company->contracts[0]->name}}"> </div>
                            </div>
                            <div class="form-group col-md-6 mb-6">
                                <h5>Fecha<span class="text-danger">*</span></h5>
                                <div class="controls">
                                <input type="date" class="form-control " id="date" name="date" required value="{{$company->contracts[0]->date}}"></div>
                            </div>

                            <div class="form-group col-md-12 mb-6">
                                <div class="form-group">
                                    <h5>Descripción<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                    <textarea name="description" id="description" rows="4" class="form-control" required>{{$company->contracts[0]->description}}</textarea>
                                </div>
                            </div>       
                        </div>
                        </div>
                        <div class="form-row ">
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-right">
                              <button type="button" class="btn btn-rounded btn-block btn-outline-info" id="agregarImagen">Agregar imagen</button>
                              <h4 class="card-title"> </h4>
                            </div>
                          </div>
                          <div class="form-row " id="imagens">
                          @foreach ($company->contracts[0]->imagens as $imagen)
                              <div class="form-group col-lg-3 col-md-4 mb-6">
                              <input type="file" id="input-file-now" name="image[]"  class="dropify form-control" data-default-file="{{asset('images/contracts/'. $imagen->name)}}" />      
                        </div>
                        @endforeach
                    </div>
                    </section>
                    <!-- Step 4 -->
                    <h6>Cuenta Bancaria</h6>
                    <section>
                        <div class="row">
                            <div class="form-group col-md-12 mb-6">
                                <h5>Cuenta bancaria de la empresa <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <select name="bank_account_id" id="bank_account_id" required class="form-control" >
                                        <option value="">Seleccione la cuenta bancaria de la empresa</option>
                                        @foreach ($bankAccounts as $item)
                                        @if($item->id == $company->bankAccounts[0]->id)
                                        <option value="{{$item->id}}" selected>{{$item->number}} - {{$item->holder}}</option>
                                     @else
                                     <option value="{{$item->id}}">{{$item->number}} - {{$item->holder}}</option>
                                     @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </section>
                    @include('alerts.modals.confirmation')
                {{-- </form> --}}
                {!!Form::close()!!}
            </div>
        </div>
    </div>
</div>
{{-- /Contenido --}}


@endsection
@section('js')
<!--Custom JavaScript -->
<script src="{{asset('template/vendor/moment/moment.js')}}"></script>
<!--/Custom JavaScript -->

{{-- wizard --}}
<script src="{{asset('template/vendor/wizard/jquery.steps.min.js')}}"></script>
<script src="{{asset('template/vendor/wizard/jquery.validate.min.js')}}"></script>
<script src="{{asset('template/vendor/wizard/steps.js')}}"></script>
{{-- /wizard --}}

 {{-- Se encarga de cambiar al español los mensajes de validacion --}}
<script src="{{asset('js/validator-messages-jquery.js')}}"></script>
 {{-- /Se encarga de cambiar al español los mensajes de validacion --}}

 {{-- Consulta los datos del departamento segun el ID de la Ciudad Seleecionado --}}
 <script src="{{asset('js/selectdinamico.js')}}"></script>
 {{-- /Consulta los datos del departamento segun el ID de la Ciudad Seleecionado --}}
 <!-- jQuery file upload -->
<script src="{{asset('template/vendor/dropify/dist/js/dropify.min.js')}}"></script>
<!-- /jQuery file upload -->

<!--- Upload--->
<script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>
     <!--- /Upload--->
@endsection