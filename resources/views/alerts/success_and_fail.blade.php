@if(Session::has('success'))
    <div class="alert alert-success"> <i class="ti-hand-point-right"></i> {{Session::get('success')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    </div>
@endif
@if(Session::has('fail'))
    <div class="alert alert-danger"> <i class="ti-hand-point-right"></i>  {{Session::get('fail')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    </div>
@endif