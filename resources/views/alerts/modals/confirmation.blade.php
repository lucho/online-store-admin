
<div class="modal fade" id="modalConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
aria-hidden="true">
<div class="modal-dialog modal-notify modal-success" role="document">
  <!--Content-->
  <div class="modal-content">
    <!--Header-->
    <div class="modal-header modal-header-info">
      <p class="heading lead"> <i class="ti-help md-3D"></i></p>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true" class="white-text">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <div class="row">
        <div class="col-3 text-center">
          <img src="{{asset('images/users/'. ((auth()->user()->imagen) ? auth()->user()->imagen : 'sin_foto.jpg'))}}" alt="" class="img-fluid z-depth-1-half rounded-circle">
          <div style="height: 10px"></div>
          <p class="title mb-0">{{ auth()->user()->name }}</p>
          <p class="text-muted " style="font-size: 13px"> <i class="ti-id-badge"></i></p>
        </div>
        <div class="col-9">
          <p>Toda información que ingrese al sistema, es importante que sea correcta.
          </p>
          <p class="card-text">
            <strong>Teniendo en cuenta lo anterior;  {{ auth()->user()->name }}  . Estas segur@  de realizar el REGISTRO</strong>
          </p>
        </div>
      </div>
    </div>
    <!--Footer-->
    <div class="modal-footer justify-content-center">
      <button type="button" class=" btn btn-outline-success waves-effect" data-dismiss="modal">NO</button>
      <button type="submit" class="btn btn-success">SI</button>
    </div>
  </div>
  <!--/.Content-->
</div>
</div>
<!-- Central Modal Medium Success-->