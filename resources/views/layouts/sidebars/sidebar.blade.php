<aside class="left-sidebar">
    <div class="d-flex no-block nav-text-box align-items-center">
        <span><img src="{{asset('template/images/logo-icon.png')}}" alt="elegant admin template"></span>
        <a class="nav-lock waves-effect waves-dark ml-auto hidden-md-down" href="javascript:void(0)"><i class="mdi mdi-toggle-switch"></i></a>
        <a class="nav-toggler waves-effect waves-dark ml-auto hidden-sm-up" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
    </div>
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav"> 
                <li class="nav-item">
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false" data-toggle="collapse" data-target="#submenu1"><i class="icon-lock"></i> <span class="hide-menu">Seguridad</span></a>
                    <div class="collapse" id="submenu1" aria-expanded="false">
                        <ul class="flex-column pl-2 nav">
                            <li class="nav-item">
                                <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false" data-toggle="collapse" data-target="#submenu1sub1"><i class="icon-people"></i><span class="hide-menu">Usuarios</span></a>
                                <div class="collapse" id="submenu1sub1" aria-expanded="false">
                                    <ul class="flex-column nav pl-4">
                                        <li class="nav-item">
                                            <a href="{{ route('users.index') }}">
                                                <i class="icon-list"></i> Lista </a>
                                        </li>
                                    </div>
                                </ul>  
                            </div>  
                        </li>
                        <li class="nav-item">
                            <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false" data-toggle="collapse" data-target="#submenu2"><i class="ti-layers-alt"></i><span class="hide-menu">Parámetros </span></a>
                            <div class="collapse" id="submenu2" aria-expanded="false">
                                <ul class="flex-column pl-2 nav">
                                    <li class="nav-item">
                                        <a href="{{ route('countrys.index') }}">Paises <i class="ti-layout-accordion-separated"></i></a>
                                        <a href="{{ route('departments.index') }}">Departamentos <i class="ti-layout-accordion-merged"></i></a>
                                        <a href="{{ route('citys.index') }}">Ciudades <i class="ti-layout-accordion-list"></i></a>
                                        <a href="{{ route('currencyttypes.index') }}">Monedas<i class="ti-money"></i></a>
                                        <a href="{{ route('trms.index') }}">Trm <i class="ti-stats-down"></i></a>
                                        <a href="{{ route('banks.index') }}">Bancos <i class="ti-layout-grid2-thumb"></i></a>
                                        <a href="{{ route('bankAccounts.index') }}">Cuentas Bancaria <i class="ti-layout-cta-right"></i></a>
                                        <a href="{{ route('paymenttypes.index') }}">Formas de Pago <i class="ti-credit-card"></i></a>
                                    </li>
                                </ul>
                            </div>    
                        </li>
                        <li class="nav-item">
                            <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false" data-toggle="collapse" data-target="#submenu3"><i class="ti-layout-grid2"></i><span class="hide-menu">Terceros </span></a>
                            <div class="collapse" id="submenu3" aria-expanded="false">
                                <ul class="flex-column pl-2 nav">
                                    <li class="nav-item">
                                        <a href="{{ route('companys.index') }}">Empresas <i class="ti-wallet"></i></a>
                                        <a href="{{ route('Conveyors.index') }}">Trasportadoras <i class="ti-truck"></i></a>
                                        <a href="#">Proveedores <i class="ti-thumb-up"></i></a>
                                    </li>
                                </ul>
                            </div>    
                        </li>
             </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>