@extends('layouts.app')

@section('css')
<link href="{{asset('template/vendor/dropify/dist/css/dropify.min.css')}}" rel="stylesheet">
<link href="{{asset('template/vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
@endsection

@section('content')
{{-- Titulo --}}
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Parámetros</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">FORMAS DE PAGO</a></li>
                <li class="breadcrumb-item active"><a href="javascript:void(0)">Crear</a></li>
            </ol>
        </div>
    </div>
</div>
{{-- Titulo --}}
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Registro de Forma de pago</h4>
                <h6 class="card-subtitle">Los campos que tienen el * son campos OBLIGATORIOS</h6>
                {!!Form::open(array('url'=>'paymenttypes','method'=>'POST','autocomplete'=>'off', 'novalidate'))!!}
                {{Form::token()}}
                <div class="form-row">
                <div class="form-group col-md-4 mb-6">
                    <h5>Nombre<span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="name" class="form-control" required data-validation-required-message="Campo requerido">
                        </div>
                    </div>
                <div class="text-xs-center">
                    <button type="submit" class="btn btn-info">Registrar</button>
                </div>
            </div>
        </div>
        {!!Form::close()!!}
    </div>
</div>




</div>
</div>
@endsection

@section('js')
<script src="{{asset('template/js/pages/jasny-bootstrap.js')}}"></script>
<script src="{{asset('template/js/pages/validation.js')}}"></script>
<!-- jQuery file upload -->
<script src="{{asset('template/vendor/dropify/dist/js/dropify.min.js')}}"></script>
<!-- /jQuery file upload -->
<!-- Validator-->
<script>
    ! function(window, document, $) {
        "use strict";
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }(window, document, jQuery);
</script>
<!--/ Validator-->
<!--- Upload--->
<script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
</script>
<!--- /Upload--->

<!-- DataPickerts -->
<script src="{{asset('template/vendor/moment/moment.js')}}"></script>
<script src="{{asset('template/vendor/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
<!-- Date range Plugin JavaScript -->

<!-- /DataPickerts -->

<script>
    // MAterial Date picker    
    $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    // Clock pickers
    $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });
    $('.clockpicker').clockpicker({
        donetext: 'Done',
    }).find('input').change(function() {
        console.log(this.value);
    });
    
    if (/mobile/i.test(navigator.userAgent)) {
        $('input').prop('readOnly', true);
    }
   
    
    </script>

@endsection