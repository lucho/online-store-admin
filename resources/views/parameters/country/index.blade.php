@extends('layouts.app')
@section('css')
<link href="{{asset('template/vendor/bootstrap-table/dist/bootstrap-table.min.css')}}" rel="stylesheet">
@endsection
@section('content')
 {{-- Titulo --}}
 <div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Lista de paises</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Parámetros</a></li>
                <li class="breadcrumb-item active">Paises</li>
            </ol>
            <form action="{{ route('countrys.create') }}">
                <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-info m-l-40 " href="{{ route('countrys.create') }}"><i class="ti-plus"></i> Crear País</button>
             </form>
        </div>
    </div>
</div>
{{-- Titulo --}}

@include('alerts.success_and_fail')
@include('parameters.country.search') 
{{-- <div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <table class="tablesaw table-bordered table-hover table no-wrap" data-tablesaw-mode="swipe"
                      data-tablesaw-minimap
                    data-tablesaw-mode-switch>
                    <thead>
                        <tr>
                            
                            <th scope="col" data-tablesaw-sortable-col  class="border">
                                #</th>
                                <th scope="col" data-tablesaw-sortable-col  data-tablesaw-sortable-default-col class="border">
                                    Opciones
                                </th>
                            <th scope="col" data-tablesaw-sortable-col 
                                 class="border">Nombre</th>
                            
                            <th scope="col" data-tablesaw-sortable-col  class="border">
                                Imagen
                            </th>
                             <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class="border">Perfil
                            </th> 
                            
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <td  >{{ $user->id }}</td>
                            <td><div class="btn-group">
                                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="ti-settings"></i>
                                </button>
                                <div class="dropdown-menu animated flipInX">
                                    <a class="dropdown-item" href="{{ route('users.edit', $user) }}">Actualizar</a>
                                    <a href="" data-target="#modal-delete-{{$user->id}}"
                                                data-toggle="modal" class="dropdown-item">Inactivar</a>
                                </div>
                            </div></td>
                            <td> {{ $user->name }}</td>
                            <td>
                                <img src="{{asset('images/users/'. $user->imagen)}}" alt="Foto"
                                    height="60px" width="70px" class="img-thumbnail">
                            </td> 
                            <td>@if(count($user->roles) != 0) {{$user->roles[0]->name}} @else SIN PERFIL @endif</td>                         
                        </tr>
                        @include('parameters.country.modal') 
                    @endforeach
                    </tbody>
                </table>
                {{$users->render()}}
            </div>
        </div>
    </div>
</div> --}}

<div class="row">
    <div class="col-12">
        <!-- Table -->
        <div class="card">
            <div class="card-body">
                {{-- <h4 class="card-title">Bootstrap Simple Table</h4>
                <h6 class="card-subtitle">Simple table example</h6> --}}
                <table data-toggle="table" data-height="250" data-mobile-responsive="true"
                    class="table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($countrys as $item)
                        <tr>
                            <td  >{{ $item->id }}</td>
                            <td> {{ $item->name }}</td>
                            <td><div class="btn-group">
                                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="ti-settings"></i>
                                </button>
                                <div class="dropdown-menu animated flipInX">
                                    <a class="dropdown-item" href="{{ route('countrys.edit', $item) }}">Actualizar</a>
                                     <a href="" data-target="#modal-delete-{{$item->id}}"
                                                data-toggle="modal" class="dropdown-item">Inactivar</a> 
                                </div>
                            </div></td>
                        </tr>
                        @include('parameters.country.modal') 
                    @endforeach
                    </tbody>
                </table>
                {{$countrys->render()}}
            </div>
        </div>
        <!-- /Table -->
    </div>
</div>


               
@endsection
@section('js')
<script src="{{asset('template/vendor/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>


<script src="{{asset('template/vendor/bootstrap-table/dist/bootstrap-table.min.js')}}"></script>

{{-- <script src="{{asset('template/vendor/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>

<script src="../../../../../unpkg.com/tableexport.jquery.plugin%401.10.13/tableExport.min.js"></script>
    <script src="../assets/node_modules/bootstrap-table/dist/bootstrap-table.min.js"></script>
    <script src="../assets/node_modules/bootstrap-table/dist/bootstrap-table-locale-all.min.js"></script>
    <script src="../assets/node_modules/bootstrap-table/dist/extensions/export/bootstrap-table-export.min.js"></script>
    <script src="dist/js/pages/bootstrap-table.init.js"></script> --}}


@endsection