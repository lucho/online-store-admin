<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$item->id}}">
	{{Form::Open(array('action'=>array('Parameters\BankAccountController@destroy',$item->id),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
                <h4 class="modal-title"> @if($item->enabled == 1) Inactivar  @else Activar @endif  Banco</h4>
			</div>
			<div class="modal-body">
				<p>Confirme si desea @if($item->enabled == 1) Inactivar  @else Activar @endif la cuenta  {{$item->number}}</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>