@extends('layouts.app')
@section('css')
<link href="{{asset('template/vendor/bootstrap-table/dist/bootstrap-table.min.css')}}" rel="stylesheet">
@endsection
@section('content')
{{-- Titulo --}}
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Lista de Cuentas bancarias</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Parámetros</a></li>
                <li class="breadcrumb-item active">Cuentas Bancarias</li>
            </ol>
            <form action="{{ route('bankAccounts.create') }}">
                <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-info m-l-40 " href="{{ route('bankAccounts.create') }}"><i class="ti-plus"></i>Crear Cuenta Bancaria</button>
            </form>
        </div>
    </div>
</div>
{{-- Titulo --}}

@include('alerts.success_and_fail')
@include('parameters.bankAccount.search')
<div class="row">
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive ">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Número</th>
                                <th>Poseedor</th>
                                <th>Tipo de cuenta</th>
                                <th>Banco</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($bankAccounts as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td> {{ $item->number }}</td>
                                <td> {{ $item->holder }}</td>
                                <td> {{ $item->bankAccountsType->name }}</td>
                                <td> {{ $item->bank->name }}</td>
                                <td>@if($item->enabled == 1) ACTIVO @else INACTIVO @endif</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="ti-settings"></i>
                                        </button>
                                        <div class="dropdown-menu animated flipInX">
                                            <a class="dropdown-item" href="{{ route('bankAccounts.edit', $item) }}">Actualizar</a>
                                            <a href="" data-target="#modal-delete-{{$item->id}}"
                                                data-toggle="modal" class="dropdown-item">@if($item->enabled == 1) Inactivar  @else Activar  @endif</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @include('parameters.bankAccount.modal') 

                            @endforeach
                        </tbody>
                    </table>
                    {{$bankAccounts->render()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('template/vendor/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
<script src="{{asset('template/vendor/bootstrap-table/dist/bootstrap-table.min.js')}}"></script>
<script src="{{asset('template/vendor/peity/jquery.peity.min.js')}}"></script>
<script src="{{asset('template/vendor/peity/jquery.peity.init.js')}}"></script>

@endsection