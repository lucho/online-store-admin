@extends('layouts.app')

@section('css')
<link href="{{asset('template/vendor/dropify/dist/css/dropify.min.css')}}" rel="stylesheet">
@endsection

@section('content')
 {{-- Titulo --}}
 <div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Seguridad</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Usuarios</a></li>
                <li class="breadcrumb-item active">Actualizar</li>
            </ol>
            {{-- <button type="button" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button> --}}
        </div>
    </div>
</div>
{{-- Titulo --}}
<div class="row">
  <div class="col-12">
      <div class="card">
          <div class="card-body">
              <h4 class="card-title">Registro de usuario</h4>
              <h6 class="card-subtitle">Los campos que tienen el * son campos OBLIGATORIOS</h6>
              {!!Form::open(array('method'=>'PATCH','route'=>['users.update',$user->id],'files'=>'true'))!!}
              {{Form::token()}}
                <div class="form-row ">
                <div class=" form-group col-lg-2 col-md-4 mb-6">
                  <h4 class="card-title">Imagen</h4>
                  <input type="file" id="input-file-now" name="image"  class="dropify form-control" data-default-file="{{asset('images/users/'. $user->imagen)}}" />      
              </div>
                </div>
              <div class="form-row">
                  <div class="form-group col-md-6 mb-12">
                      <h5>Nombre<span class="text-danger">*</span></h5>
                      <div class="controls">
                          <input type="text" name="name" class="form-control" required data-validation-required-message="Campo requerido" value="{{$user->name}}"> </div>
                      {{-- <div class="form-control-feedback"><small>Add <code>Requerido</code> attribute to field for required validation.</small></div> --}}
                  </div>
                  <div class="form-group col-md-6 mb-12">
                      <h5>Correo<span class="text-danger">*</span></h5>
                      <div class="controls">
                          <input type="email" name="email" class="form-control" required data-validation-required-message="Campo requerido" data-validation-email-message="Campo invalido" value="{{$user->email}}"> </div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-6 mb-12">
                    <h5>Contraseña<span class="text-danger">*</span></h5>
                    <div class="controls">
                        <input type="password" name="password" class="form-control" required data-validation-required-message="Campo requerido" value="{{$user->password}}"> </div>
                </div>
                <div class="form-group col-md-6 mb-12">
                    <h5>Confirmar  contraseña <span class="text-danger">*</span></h5>
                    <div class="controls">
                        <input type="password" name="password2" data-validation-match-match="password" data-validation-match-message=" Las contraseñas no coinciden" class="form-control" required  data-validation-required-message="Que no se te olvide"  value="{{$user->password}}"> </div>
                </div>

                </div>
                <div class="form-row">
                <div class="form-group col-md-6 mb-12">
                  <h5>Perfil <span class="text-danger">*</span></h5>
                  <div class="controls">
                      <select name="role_id" id="select" required class="form-control" data-validation-required-message="Campo requerido">
                        @if(count($user->roles) != 0) 
                          @foreach ($roles as $rol)
                          @if($rol->id == $user->roles[0]->id)
                        <option value="{{$rol->id}}" selected>{{$rol->name}}</option>
                        @else
                        <option value="{{$rol->id}}">{{$rol->name}}</option>
                        @endif
                          @endforeach
                        @else 
                        <option value="">Seleccione un perfil</option>
                          @foreach ($roles as $rol)
                        <option value="{{$rol->id}}">{{$rol->name}}</option>
                          @endforeach
                        @endif
                      </select>
                  </div>
              </div>
              <div class="form-group col-md-6 mb-12">
                <h5>Empresa <span class="text-danger">*</span></h5>
                <div class="controls">
                    <select name="company_id" id="select" required class="form-control" data-validation-required-message="Campo requerido">
                        {{-- <option value="">Seleccione la empresa a la cual pertenece el usuario</option> --}}
                        @foreach ($companys as $company)
                        @if($company->id == $user->company_id)
                        <option value="{{$company->id}}" selected>{{$company->name}}</option>
                        @else
                        <option value="{{$company->id}}">{{$company->name}}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
            </div>
            </div>                
                  <div class="text-xs-right">
                      <button type="submit" class="btn btn-info">Registrar</button>
                  </div>                <!-- End SmartWizard Content -->
                {!!Form::close()!!}
          </div>
      </div>
  </div>
</div>
@endsection

@section('js')
<script src="{{asset('template/js/pages/jasny-bootstrap.js')}}"></script>
<script src="{{asset('template/js/pages/validation.js')}}"></script>
<!-- jQuery file upload -->
<script src="{{asset('template/vendor/dropify/dist/js/dropify.min.js')}}"></script>
<!-- /jQuery file upload -->
<!-- Validator-->
 <script>
  ! function(window, document, $) {
      "use strict";
      $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
  }(window, document, jQuery);
  </script>
  <!--/ Validator-->
  <!--- Upload--->
   <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>
     <!--- /Upload--->
@endsection
