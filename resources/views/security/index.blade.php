@extends('layouts.app')
@section('css')
<link href="{{asset('template/vendor/tablesaw/dist/tablesaw.css')}}" rel="stylesheet">
@endsection
@section('content')
 {{-- Titulo --}}
 <div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Lista de usuarios</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Seguridad</a></li>
                <li class="breadcrumb-item ">Usuarios</li>
                <li class="breadcrumb-item active">Lista</li>
            </ol>
            <form action="{{ route('users.create') }}">
                <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-info m-l-40 " href="{{ route('users.create') }}"><i class="ti-plus"></i> Crear Usuario</button>
             </form>
        </div>
    </div>
</div>
{{-- Titulo --}}

<!-- {{-- <div class="card-group">
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Listado <small> Estudiantes</small></h3>
            </div>

             @include('security.search')
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Maestros <small>Aprendices</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                        <th class="column-title">Imagen</th>
                                        <th class="column-title">Nombre </th>
                                        <th class="column-title no-link last"><span class="nobr">Opciones</span>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach ($users as $user)
                                    <tr class="even pointer">
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->imagen}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>
                                          
                                            <a href="detailperson/{{$items->id}}"><button type="button"
                                                    class="btn btn-round bg-purple"><i
                                                        class="fa fa-wpforms"></i></button></a> 
                                        </td>
                                    </tr>
                                    @include('persona.modal') 
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{$users->render()}}
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
</div> --}} -->
@if(Session::has('success'))
    <div class="alert alert-success"> <i class="ti-user"></i> {{Session::get('success')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    </div>
@endif
@if(Session::has('fail'))
    <div class="alert alert-danger"> <i class="ti-user"></i>  {{Session::get('fail')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    </div>
@endif
    @include('security.search')
<div class="row">
    <div class="col-12">
        <!-- Column -->
        <div class="card">
            <div class="card-body">
                {{-- <h4 class="card-title">Kitchen Sink</h4>
                <h5 class="card-subtitle">Swipe Mode, ModeSwitch, Minimap, Sortable, SortableSwitch</h5> --}}
                <table class="tablesaw table-bordered table-hover table no-wrap" data-tablesaw-mode="swipe"
                      data-tablesaw-minimap
                    data-tablesaw-mode-switch>
                    <thead>
                        <tr>
                            
                            <th scope="col" data-tablesaw-sortable-col  class="border">
                                #</th>
                                <th scope="col" data-tablesaw-sortable-col  data-tablesaw-sortable-default-col class="border">
                                    Opciones
                                </th>
                            <th scope="col" data-tablesaw-sortable-col 
                                 class="border">Nombre</th>
                            {{-- <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2" class="border">Year
                            </th> --}}
                            <th scope="col" data-tablesaw-sortable-col  class="border">
                                Imagen
                            </th>
                             <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4" class="border">Perfil
                            </th> 
                            
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <td  >{{ $user->id }}</td>
                            <td><div class="btn-group">
                                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="ti-settings"></i>
                                </button>
                                <div class="dropdown-menu animated flipInX">
                                    <a class="dropdown-item" href="{{ route('users.edit', $user) }}">Actualizar</a>
                                    <a href="" data-target="#modal-delete-{{$user->id}}"
                                                data-toggle="modal" class="dropdown-item">Inactivar</a>

                                    {{-- <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a> --}}
                                </div>
                            </div></td>
                            <td> {{ $user->name }}</td>
                            <td>
                                <img src="{{asset('images/users/'. $user->imagen)}}" alt="Foto"
                                    height="60px" width="70px" class="img-thumbnail">
                            </td> 
                            <td>@if(count($user->roles) != 0) {{$user->roles[0]->name}} @else SIN PERFIL @endif</td>                         
                        </tr>
                        @include('security.modal') 
                    @endforeach
                    </tbody>
                </table>
                {{$users->render()}}
            </div>
        </div>
        <!-- Column -->
        
    </div>
</div>



               
@endsection
@section('js')
<script src="{{asset('template/vendor/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
<!-- {{-- <script src="{{asset('template/vendor/jquery-datatables-editable/jquery.dataTables.js')}}"></script>
<script src="{{asset('template/vendor/datatables/media/js/dataTables.bootstrap.html')}}"></script>
<script src="{{asset('template/vendor/tiny-editable/mindmup-editabletable.js')}}"></script>
<script src="{{asset('template/vendor/tiny-editable/numeric-input-example.js')}}"></script> --}} -->

<script src="{{asset('template/vendor/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jQuery peity -->
<script src="{{asset('template/vendor/tablesaw/dist/tablesaw.jquery.js')}}"></script>
<script src="{{asset('template/vendor/tablesaw/dist/tablesaw-init.js')}}"></script>


 <!-- Chart JS -->
 <!-- {{-- <script>
     var idioma_español = {
                 "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla =(",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
}
 $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
$('#editable-datatable').editableTableWidget().numericInputExample().find('td:first').focus();
$(function() {
$('#editable-datatable').DataTable(
    {
            "language": idioma_español
        }
);
});
   
 </script> --}} -->
@endsection