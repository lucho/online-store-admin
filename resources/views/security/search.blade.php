{!! Form::open(array('url'=>'users','method'=>'GET','autocomplete'=>'off','role'=>'search')) !!}
{{-- <div class="form-group">
	<div class="input-group">
		<input type="text"  class="form-control" name="searchText" placeholder="Buscar..." value="{{$searchText}}">
		<span class="input-group-btn">
			<button type="submit"  class="btn btn-primary"> Buscar</button>
		</span>
		
    </div>
	
</div> --}}
<div class="row">
    <div class="col-12">
        <!-- Column -->
        <div class="card">
            <div class="card-body">
                <div class="input-group"> 
                            <input type="text"  name="searchText" class="form-control" placeholder="Buscar... " value="{{$searchText}}"> <a class="srh-btn"></a>
                            <span class="input-group-btn">
                                <span style="color:#FFFFFF;">...</span><button type="submit" class="btn btn-warning btn-circle btn-ms"> <i class="ti-search"></i></button>
                            </span>
                </div>
            </div>
        </div>
    </div>
</div>

{{Form::close()}}