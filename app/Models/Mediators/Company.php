<?php

namespace App\Models\Mediators;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\SoftCascadeTrait;
use App\Models\Parameters\{City, IdentificationsType, BankAccount};
use App\Models\Mediators\{Contact, Contract};

class Company extends Model
{
    use SoftDeletes;
    protected $table = "companys";
    protected $softCascade = [];
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name',
        'identifications_type_id',
        'document_number',
        'email',
        'phone',
        'direction',
        'city_id',
        'enabled',
    ];


    public function city()
    {
        return $this->belongsTo(City::class);
    }
    public function identificationsType()
    {
        return $this->belongsTo(IdentificationsType::class);
    }
    public function contacts()
    {
        return $this->belongsToMany(Contact::class);
    }
    public function bankAccounts()
    {
        return $this->belongsToMany(BankAccount::class);
    }
    public function contracts()
    {
        return $this->hasMany(Contract::class);
    }
}
