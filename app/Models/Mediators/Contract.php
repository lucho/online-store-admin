<?php

namespace App\Models\Mediators;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\SoftCascadeTrait;
use App\Models\Mediators\{Company,Image};

class Contract extends Model
{
    use SoftDeletes;
    protected $softCascade = [];
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name',
        'date',
        'contracts_type_id',
        'company_id',
        'description',  
        'enabled',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
    public function imagens()
    {
        return $this->belongsToMany(Image::class);
    }
}
