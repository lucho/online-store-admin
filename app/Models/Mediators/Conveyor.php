<?php

namespace App\Models\Mediators;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\SoftCascadeTrait;
use App\Models\Parameters\{IdentificationsType};

class Conveyor extends Model
{
    use SoftDeletes;
    protected $table = "conveyors";
    protected $softCascade = [];
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name',
        'identifications_type_id',
        'document_number',
        'email',
        'phone',
        'direction',
        'enabled',
    ];
    public function identificationsType()
    {
        return $this->belongsTo(IdentificationsType::class);
    }
}
