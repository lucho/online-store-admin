<?php

namespace App\Models\Mediators;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\SoftCascadeTrait;
use App\Models\Mediators\{Company};

class Contact extends Model
{
    use SoftDeletes;
    protected $softCascade = [];
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name',
        'email',
        'phone',
        'enabled',
    ];

    public function companys()
    {
        return $this->belongsToMany(Company::class);
    }
}
