<?php

namespace App\Models\Mediators;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\SoftCascadeTrait;
use App\Models\Mediators\{Contract};

class Image extends Model
{
    use SoftDeletes;
    protected $softCascade = [];
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name',
        'url',
        'type',
        'size',
        'enabled'
    ];

    public function contracts()
    {
        return $this->belongsToMany(Contract::class);
    }
}
