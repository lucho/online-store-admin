<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\SoftCascadeTrait;

class Trm extends Model
{
    use SoftDeletes;
    protected $softCascade = [];
    protected $dates = ['deleted_at'];
    protected $fillable = [
            'date',
            'value',
            'is_enabled'
    ];
}
