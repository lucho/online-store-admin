<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\SoftCascadeTrait;

class Country extends Model
{
    use SoftDeletes;
    protected $softCascade = [];
    protected $table = "countrys";
    protected $dates = ['deleted_at'];
    protected $fillable = [
            'name',
            'enabled'
    ];

    public function departments(){
        return $this->hasMany(Department::class);
    }
}
