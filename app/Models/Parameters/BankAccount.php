<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\SoftCascadeTrait;
use App\Models\Parameters\{Bank, BankAccountsTypes};
use App\Models\Mediators\{Company};

class BankAccount extends Model
{
    use SoftDeletes;
    protected $softCascade = [];
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'number',
        'holder',
        'bank_accounts_type_id',
        'bank_id',
        'enabled'
    ];

    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }
    public function bankAccountsType()
    {
        return $this->belongsTo(BankAccountsTypes::class);
    }
    public function companys()
    {
        return $this->belongsToMany(Company::class);
    }
}
