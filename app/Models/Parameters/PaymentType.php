<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\SoftCascadeTrait;
use App\Models\Mediators\{Supplier};

class PaymentType extends Model
{
    use SoftDeletes;
    protected $table = "payment_types";
    protected $softCascade = [];
    protected $dates = ['deleted_at'];
    protected $fillable = [
            'name',
            'is_enabled'
    ];

    public function Product()
    {
        return $this->belongsTo(Supplier::class);
    }
}