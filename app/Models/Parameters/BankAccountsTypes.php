<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\SoftCascadeTrait;
use App\Models\Parameters\{BankAccount};


class BankAccountsTypes extends Model
{
    use SoftDeletes;
    protected $softCascade = [];
    protected $dates = ['deleted_at'];
    protected $fillable = [
            'name',
            'enabled'
    ];

    public function bankAccounts(){
        return $this->hasMany(BankAccount::class);
    }
}
