<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;
use App\Models\Mediators\{Company};

class IdentificationsType extends Model
{
    protected $table = "identifications_types";
    protected $fillable = [
        'id',
        'name',
        'enabled',
        'created_at',
        'updated_at'
    ];

    public function companys()
  {
    return $this->hasMany(Company::class);
  }
}
