<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\SoftCascadeTrait;
use App\Models\Parameters\{City,Country};

class Department extends Model
{
    use SoftDeletes;
    protected $softCascade = [];
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'country_id',
        'is_enabled',
    ];

    public function Citys()
    {
        return $this->hasMany(City::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
