<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\SoftCascadeTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Parameters\{Department};
use App\Models\Mediators\{Company};


class City extends Model
{
  use SoftDeletes;

  protected $table = "citys";
  protected $softCascade = [];
  protected $dates = ['deleted_at'];
  protected $fillable = [
    'name',
    'department_id',
    'is_enabled',
  ];

  public function department()
  {
    return $this->belongsTo(Department::class);
  }
  
  public function companys()
  {
    return $this->hasMany(Company::class);
  }
}
