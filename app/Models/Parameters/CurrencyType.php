<?php

namespace App\Models\Parameters;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\SoftCascadeTrait;
use App\Models\Products\{Product};

class CurrencyType extends Model
{
    use SoftDeletes;
    protected $softCascade = [];
    protected $dates = ['deleted_at'];
    protected $fillable = [
            'name',
            'is_enabled'
    ];

    public function Product()
    {
        return $this->belongsTo(Product::class);
    }
}
