<?php

namespace App\Models\Security;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\SoftCascadeTrait;
use App\Models\Security\{User};

class Role extends Model
{
    use SoftDeletes;
    protected $softCascade = [];
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id',
        'name',
        'enabled',
        'created_at',
        'updated_at'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class); // De muchos a muchos con Users
    }

}
