<?php

namespace App\Models\Security;

use Illuminate\Database\Eloquent\Model;
use App\Models\Security\{Role};
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\SoftCascadeTrait;

class User extends Model
{
    use SoftDeletes;
    protected $softCascade = [];

    protected $fillable = [
        'id',
        'name',
        'email',
        'email_verified_at',
        'password',
        'imagen',
        'company_id',
        'enabled',
        'remember_token',
        'created_at',
        'updated_at'
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class); // De muchos a muchos con Roles
    }
}
