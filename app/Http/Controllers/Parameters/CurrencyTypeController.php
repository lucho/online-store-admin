<?php

namespace App\Http\Controllers\Parameters;

use App\Http\Controllers\Controller;
use App\Models\Parameters\CurrencyType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class CurrencyTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = trim($request->get('searchText'));
        $currencyttype = CurrencyType::where('name', 'LIKE', '%' . $query . '%')->paginate(5);
        return view('parameters.currencyttype.index', ['currencyttypes' => $currencyttype, 'searchText' => $query]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('parameters.currencyttype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currencyttype = new CurrencyType();
        try {
            $this->validate($request, [
                'name'=>'required|unique:currency_types',
                ]);
            $currencyttype->create($request->all());
        } catch (\Exception $e) {
            return Redirect::to("/currencyttypes")->withFail('La Moneda ya se encuentra Registrada');
        }
        return Redirect::to("/currencyttypes")->withSuccess('Registro Exitoso');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Parameters\CurrencyType  $currencyType
     * @return \Illuminate\Http\Response
     */
    public function show(CurrencyType $currencyType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Parameters\CurrencyType  $currencyType
     * @return \Illuminate\Http\Response
     */
    public function edit(CurrencyType $currencyttype)
    {
        return view('parameters.currencyttype.edit', ['currencyttypes' => $currencyttype]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Parameters\CurrencyType  $currencyType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CurrencyType $currencyttype)
    {
        try {
            $currencyttype->update($request->all());
        } catch (\Exception $e) {
            return Redirect::to("/currencyttypes")->withFail($e->getMessage());
        }
        return Redirect::to("/currencyttypes")->withSuccess('Actualización Exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Parameters\CurrencyType  $currencyType
     * @return \Illuminate\Http\Response
     */
    public function destroy(CurrencyType $currencyttype)
    {
        try {
            $currencyttype->delete();
        } catch (\Exception $e) {
            return Redirect::to("/currencyttypes")->withFail($e->getMessage());
        }
        return Redirect::to("/currencyttypes")->withSuccess('Moneda Inactivado');
    }
}
