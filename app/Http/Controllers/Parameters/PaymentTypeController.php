<?php

namespace App\Http\Controllers\Parameters;

use App\Http\Controllers\Controller;
use App\Models\Parameters\PaymentType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class PaymentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = trim($request->get('searchText'));
        $PaymentType = PaymentType::where('name', 'LIKE', '%' . $query . '%')->paginate(5);
        return view('parameters.paymenttype.index', ['PaymentType' => $PaymentType, 'searchText' => $query]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('parameters.paymenttype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $PaymentType = new PaymentType();
        try {
            $this->validate($request, [
                'name'=>'required|unique:payment_types',
                ]);
            $PaymentType->create($request->all());
        } catch (\Exception $e) {
            return Redirect::to("/paymenttypes")->withFail('La Forma de pago ya se encuentra Registrada');
        }
        return Redirect::to("/paymenttypes")->withSuccess('Registro Exitoso');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Parameters\PaymentType  $paymentType
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentType $paymentType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Parameters\PaymentType  $paymentType
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentType $Paymenttype)
    {
        return view('parameters.paymenttype.edit', ['PaymentType' => $Paymenttype]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Parameters\PaymentType  $paymentType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentType $Paymenttype)
    {
        try {
            $Paymenttype->update($request->all());
        } catch (\Exception $e) {
            return Redirect::to("/paymenttypes")->withFail($e->getMessage());
        }
        return Redirect::to("/paymenttypes")->withSuccess('Actualización Exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Parameters\PaymentType  $paymentType
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentType $Paymenttype)
    {
        try {
            $Paymenttype->delete();
        } catch (\Exception $e) {
            return Redirect::to("/paymenttypes")->withFail($e->getMessage());
        }
        return Redirect::to("/paymenttypes")->withSuccess('Forma de pago Inactivado');
    }
}
