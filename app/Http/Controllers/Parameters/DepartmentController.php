<?php

namespace App\Http\Controllers\Parameters;

use App\Http\Controllers\Controller;
use App\Models\Parameters\Country;
use App\Models\Parameters\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = trim($request->get('searchText'));
        $departments = Department::with('country')->where('name', 'LIKE', '%' . $query . '%')->paginate(5);
        return view('parameters.department.index', ['departments' => $departments, 'searchText' => $query]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countrys = Country::Where('enabled', 1)->get();
        return view('parameters.department.create', ['countrys' => $countrys]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $department = new Department();
        try {
            $department->create($request->all());
        } catch (\Exception $e) {
            return Redirect::to("/departments")->withFail($e->getMessage());
        }
        return Redirect::to("/departments")->withSuccess('Registro Exitoso');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Parameters\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Parameters\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        $countrys = Country::Where('enabled', 1)->get();
        return view('parameters.department.edit', ['department' => $department, 'countrys' => $countrys]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Parameters\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        try {
            $department->update($request->all());
        } catch (\Exception $e) {
            return Redirect::to("/departments")->withFail($e->getMessage());
        }
        return Redirect::to("/departments")->withSuccess('Actualización Exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Parameters\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        try {
            $department->delete();
        } catch (\Exception $e) {
            return Redirect::to("/departments")->withFail($e->getMessage());
        }
        return Redirect::to("/departments")->withSuccess('Departamento Inactivado');
    }

    /**
     * Realiza la consulta del departamento Segun el id del país
     */
    
    public function getDepartmentsByCountry(Request $request,$old_country_id,  $country_id = null)
    {
        $parametro = $country_id;
        if(!$country_id){
            $parametro = $old_country_id;
        }
        $departments = Department::Where('country_id', $parametro)->get();
        return $departments;
    }
}
