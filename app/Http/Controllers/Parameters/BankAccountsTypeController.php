<?php

namespace App\Http\Controllers\Parameters;

use App\Http\Controllers\Controller;
use App\Models\Parameters\BankAccountsTypes;
use Illuminate\Http\Request;

class BankAccountsTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Parameters\BankAccountsTypes  $bankAccountsTypes
     * @return \Illuminate\Http\Response
     */
    public function show(BankAccountsTypes $bankAccountsTypes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Parameters\BankAccountsTypes  $bankAccountsTypes
     * @return \Illuminate\Http\Response
     */
    public function edit(BankAccountsTypes $bankAccountsTypes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Parameters\BankAccountsTypes  $bankAccountsTypes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BankAccountsTypes $bankAccountsTypes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Parameters\BankAccountsTypes  $bankAccountsTypes
     * @return \Illuminate\Http\Response
     */
    public function destroy(BankAccountsTypes $bankAccountsTypes)
    {
        //
    }
}
