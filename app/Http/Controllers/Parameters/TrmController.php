<?php

namespace App\Http\Controllers\Parameters;

use App\Http\Controllers\Controller;
use App\Models\Parameters\Trm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class TrmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = trim($request->get('searchText'));
        $trms = Trm::where('date', 'LIKE', '%' . $query . '%')->paginate(5);
        return view('parameters.trm.index', ['trms' => $trms, 'searchText' => $query]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('parameters.trm.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $trm = new Trm();
        try {
            $trm->create($request->all());
        } catch (\Exception $e) {
            return Redirect::to("/trms")->withFail($e->getMessage());
        }
        return Redirect::to("/trms")->withSuccess('Registro Exitoso');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Parameters\Trm  $trm
     * @return \Illuminate\Http\Response
     */
    public function show(Trm $trm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Parameters\Trm  $trm
     * @return \Illuminate\Http\Response
     */
    public function edit(Trm $trm)
    {
        return view('parameters.trm.edit', ['trm' => $trm]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Parameters\Trm  $trm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trm $trm)
    {
        try {
            $trm->update($request->all());
        } catch (\Exception $e) {
            return Redirect::to("/trms")->withFail($e->getMessage());
        }
        return Redirect::to("/trms")->withSuccess('Actualización Exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Parameters\Trm  $trm
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trm $trm)
    {
        try {
            $trm->delete();
        } catch (\Exception $e) {
            return Redirect::to("/trms")->withFail($e->getMessage());
        }
        return Redirect::to("/trms")->withSuccess('TRM Inactivado');
    }
}
