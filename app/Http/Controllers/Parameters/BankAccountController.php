<?php

namespace App\Http\Controllers\Parameters;

use App\Http\Controllers\Controller;
use App\Models\Parameters\BankAccount;
use Illuminate\Http\Request;
use App\Models\Parameters\{Bank, BankAccountsTypes};
use Illuminate\Support\Facades\Redirect;

class BankAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = trim($request->get('searchText'));
        $bankAccounts = BankAccount::with('bank','bankAccountsType')->where('number', 'LIKE', '%' . $query . '%')->paginate(5);
        //dd($bankAccounts);
        return view('parameters.bankAccount.index', ['bankAccounts' => $bankAccounts, 'searchText' => $query]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $banks = Bank::Where('enabled', 1)->get();
        $bankAccountsTypes = BankAccountsTypes::Where('enabled', 1)->get();
        return view('parameters.bankAccount.create', ['banks' => $banks, 'bankAccountsTypes' => $bankAccountsTypes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bankAccount = new BankAccount();
        try {
            $bankAccount->create($request->all());
        } catch (\Exception $e) {
            return Redirect::to("/bankAccounts")->withFail($e->getMessage());
        }
        return Redirect::to("/bankAccounts")->withSuccess('Registro Exitoso');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Parameters\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function show(BankAccount $bankAccount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Parameters\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function edit(BankAccount $bankAccount)
    {
        $bankAccount = BankAccount::with('bank','bankAccountsType')->where('id', $bankAccount->id)->first();
        $banks = Bank::Where('enabled', 1)->get();
        $bankAccountsTypes = BankAccountsTypes::Where('enabled', 1)->get();
        return view('parameters.bankAccount.edit', ['bankAccount' => $bankAccount, 'banks' => $banks, 'bankAccountsTypes' => $bankAccountsTypes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Parameters\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BankAccount $bankAccount)
    {
        try {
            $bankAccount->update($request->all());
        } catch (\Exception $e) {
            return Redirect::to("/bankAccounts")->withFail($e->getMessage());
        }
        return Redirect::to("/bankAccounts")->withSuccess('Actualización Exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Parameters\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function destroy(BankAccount $bankAccount)
    {

        try {
            $bankAccount->enabled = ($bankAccount->enabled === 1 ? 2 : 1);
            $bankAccount->update();
        } catch (\Exception $e) {
            return Redirect::to("/bankAccounts")->withFail($e->getMessage());
        }
        return Redirect::to("/bankAccounts")->withSuccess('Cuenta de banco Actualizada');
    }
}
