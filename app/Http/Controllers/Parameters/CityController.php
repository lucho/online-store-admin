<?php

namespace App\Http\Controllers\Parameters;

use App\Http\Controllers\Controller;
use App\Models\Parameters\City;
use App\Models\Parameters\{Country, Department};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = trim($request->get('searchText'));
        $citys = City::with('department.country')->where('name', 'LIKE', '%' . $query . '%')->paginate(5);
        return view('parameters.city.index', ['citys' => $citys, 'searchText' => $query]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countrys = Country::Where('enabled', 1)->get();
        $departments = Department::Where('enabled', 1)->get();
        return view('parameters.city.create', ['countrys' => $countrys, 'departments' => $departments]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $city = new City();
        try {
            $city->create($request->all());
        } catch (\Exception $e) {
            return Redirect::to("/citys")->withFail($e->getMessage());
        }
        return Redirect::to("/citys")->withSuccess('Registro Exitoso');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Parameters\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Parameters\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        $city = City::with('department.country')->where('id', $city->id)->first();
        $countrys = Country::Where('enabled', 1)->get();
        $departments = Department::Where('enabled', 1)->Where('country_id', $city->department->country->id)->get();
        return view('parameters.city.edit', ['city' => $city, 'countrys' => $countrys, 'departments' => $departments]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Parameters\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        try {
            $city->update($request->all());
        } catch (\Exception $e) {
            return Redirect::to("/citys")->withFail($e->getMessage());
        }
        return Redirect::to("/citys")->withSuccess('Actualización Exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Parameters\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        try {
            $city->delete();
        } catch (\Exception $e) {
            return Redirect::to("/citys")->withFail($e->getMessage());
        }
        return Redirect::to("/citys")->withSuccess('Ciudad Inactivado');
    }

     /**
     * Realiza la consulta de la ciudad Segun el id del departamento
     */
    
    public function getCitysByDepartment(Request $request,$old_department_id,  $department_id = null)
    {
        $parametro = $department_id;
        if(!$department_id){
            $parametro = $old_department_id;
        }
        $citys = City::Where('department_id', $parametro)->get();
        return $citys;
    }
}
