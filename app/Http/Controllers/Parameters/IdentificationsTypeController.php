<?php

namespace App\Http\Controllers\Parameters;

use App\Http\Controllers\Controller;
use App\Models\Parameters\IdentificationsType;
use Illuminate\Http\Request;

class IdentificationsTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Parameters\IdentificationsType  $identificationsType
     * @return \Illuminate\Http\Response
     */
    public function show(IdentificationsType $identificationsType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Parameters\IdentificationsType  $identificationsType
     * @return \Illuminate\Http\Response
     */
    public function edit(IdentificationsType $identificationsType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Parameters\IdentificationsType  $identificationsType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IdentificationsType $identificationsType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Parameters\IdentificationsType  $identificationsType
     * @return \Illuminate\Http\Response
     */
    public function destroy(IdentificationsType $identificationsType)
    {
        //
    }
}
