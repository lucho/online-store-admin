<?php

namespace App\Http\Controllers\Security;

use App\Http\Controllers\Controller;
use App\Models\Mediators\Company;
use App\Models\Security\Role;
use App\Models\Security\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = trim($request->get('searchText'));
        $user = User::with('roles')->where('name', 'LIKE', '%' . $query . '%')->paginate(5);
        return view('security.index', ['users' => $user, 'searchText' => $query]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::Where('enabled', 1)->get();
        $companys = Company::Where('enabled', 1)->get();
        return view('security.create', ['roles' => $roles, 'companys' => $companys]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        try {
            DB::beginTransaction();
            $user = $user->create($request->all());
            $user->roles()->attach($request->get('role_id'));
            $user->password = Hash::make($user->password);
            if ($request->image) {
                $request->image->move(public_path('images/users'), $user->id . '.' . $request->image->getClientOriginalExtension());
                $user->imagen = $user->id . '.' . $request->image->getClientOriginalExtension();
            }
            $user->update();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return Redirect::to("/users")->withFail($e->getMessage());
        }
        return Redirect::to("/users")->withSuccess('Registro Exitoso');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Security\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Security\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user = User::with('roles')->where('id', $user->id)->first();
        $roles = Role::Where('enabled', 1)->get();
        $companys = Company::Where('enabled', 1)->get();
        return view('security.edit', ['user' => $user, 'roles' => $roles, 'companys' => $companys]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Security\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        try {
            DB::beginTransaction();
            $user->update(
                $request->merge(['password' => Hash::make($request->get('password'))])
                    ->except(
                        [$request->get('password') ? '' : 'password']
                    )
            );
            $user = User::with('roles')->where('id', $user->id)->first();
            if ($user->company_id != $request->get('company_id')) {
                $user->company_id = $request->get('company_id');
            }
            if (count($user->roles) > 0) {
                if ($user->roles[0]->id != $request->get('role_id')) {
                    $user->roles()->sync($request->get('role_id'));
                }
            } else {
                $user->roles()->attach($request->get('role_id'));
            }
            if ($request->image) {
                $request->image->move(public_path('images/users'), $user->id . '.' . $request->image->getClientOriginalExtension());
                $user->imagen = $user->id . '.' . $request->image->getClientOriginalExtension();
            }
            $user->update();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return Redirect::to("/users")->withFail($e->getMessage());
        }
        return Redirect::to("/users")->withSuccess('Usuario actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Security\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        try {
            $user->delete();
            // $user->enabled = 2;
            // $user->update();
        } catch (\Exception $e) {
            return Redirect::to("/users")->withFail($e->getMessage());
        }
        return Redirect::to("/users")->withSuccess('Usuario Inactivado');
    }
}
