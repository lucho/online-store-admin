<?php

namespace App\Http\Controllers\Security;

use App\Http\Controllers\Controller;
use App\Models\Security\ModulesRole;
use Illuminate\Http\Request;

class ModulesRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Security\ModulesRole  $modulesRole
     * @return \Illuminate\Http\Response
     */
    public function show(ModulesRole $modulesRole)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Security\ModulesRole  $modulesRole
     * @return \Illuminate\Http\Response
     */
    public function edit(ModulesRole $modulesRole)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Security\ModulesRole  $modulesRole
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModulesRole $modulesRole)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Security\ModulesRole  $modulesRole
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModulesRole $modulesRole)
    {
        //
    }
}
