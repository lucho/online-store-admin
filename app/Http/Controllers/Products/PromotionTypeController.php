<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use App\Models\Products\PromotionType;
use Illuminate\Http\Request;

class PromotionTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Products\PromotionType  $promotionType
     * @return \Illuminate\Http\Response
     */
    public function show(PromotionType $promotionType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Products\PromotionType  $promotionType
     * @return \Illuminate\Http\Response
     */
    public function edit(PromotionType $promotionType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Products\PromotionType  $promotionType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PromotionType $promotionType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Products\PromotionType  $promotionType
     * @return \Illuminate\Http\Response
     */
    public function destroy(PromotionType $promotionType)
    {
        //
    }
}
