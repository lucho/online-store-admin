<?php

namespace App\Http\Controllers\Mediators;

use App\Http\Controllers\Controller;
use App\Models\Mediators\Conveyor;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Models\Parameters\{IdentificationsType};

use Illuminate\Support\Facades\DB;

class ConveyorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = trim($request->get('searchText'));
        $conveyors = Conveyor::with('identificationsType')->where('name', 'LIKE', '%' . $query . '%')->paginate(5);
        return view('mediators\company.conveyors.index', ['conveyors' => $conveyors, 'searchText' => $query]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $identificationsTypes = IdentificationsType::Where('enabled', 1)->get();
        return view('mediators\company.conveyors.create', ['identificationsTypes' => $identificationsTypes]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $conveyor = new conveyor();
        try {
            $conveyor->create($request->all());
        } catch (\Exception $e) {
            //DB::rollback();
            return Redirect::to("/Conveyors")->withFail($e->getMessage());
        }
        return Redirect::to("/Conveyors")->withSuccess('Registro Exitoso');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Mediators\Conveyor  $conveyor
     * @return \Illuminate\Http\Response
     */
    public function show(Conveyor $conveyor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Mediators\Conveyor  $conveyor
     * @return \Illuminate\Http\Response
     */
    public function edit(Conveyor $Conveyor)
    {
        $identificationsTypes = IdentificationsType::Where('enabled', 1)->get();
        return view('mediators\company.conveyors.edit', ['conveyor' => $Conveyor, 'identificationsTypes' => $identificationsTypes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Mediators\Conveyor  $conveyor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Conveyor $Conveyor)
    {
        try {
            $Conveyor->update($request->all());
        } catch (\Exception $e) {
            return Redirect::to("/Conveyors")->withFail($e->getMessage());
        }
        return Redirect::to("/Conveyors")->withSuccess('Actualización Exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Mediators\Conveyor  $conveyor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Conveyor $Conveyor)
    {
        try {
            $Conveyor->enabled = ($Conveyor->enabled === 1 ? 2 : 1);
            $Conveyor->update();
        } catch (\Exception $e) {
            return Redirect::to("/Conveyors")->withFail($e->getMessage());
        }
        return Redirect::to("/Conveyors")->withSuccess('Empresa Actualizada');
    }
}
