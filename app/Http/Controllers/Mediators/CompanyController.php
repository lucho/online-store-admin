<?php

namespace App\Http\Controllers\Mediators;

use App\Http\Controllers\Controller;
use App\Models\Mediators\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Models\Parameters\{IdentificationsType, Country, Department, City, BankAccount, ContractsType};
use App\Models\Mediators\{Contact, Contract, Image};

use Illuminate\Support\Facades\DB;


class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = trim($request->get('searchText'));
        $companys = Company::with('city', 'identificationsType', 'contacts', 'bankAccounts', 'contracts')->where('name', 'LIKE', '%' . $query . '%')->paginate(5);
        return view('mediators.company.index', ['companys' => $companys, 'searchText' => $query]);
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $identificationsTypes = IdentificationsType::Where('enabled', 1)->get();
        $countrys = Country::Where('enabled', 1)->get();
        $bankAccounts = BankAccount::Where('enabled', 1)->get();
        $contractsTypes = ContractsType::Where('enabled', 1)->get();
        return view('mediators.company.create', ['identificationsTypes' => $identificationsTypes, 'countrys' => $countrys, 'bankAccounts' => $bankAccounts, 'contractsTypes' => $contractsTypes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = new Company();
        $contact = new Contact();
        $contract = new Contract();
        try {
            DB::beginTransaction();
            $company = $company->create($request->all());
            $contact = $contact->create(['name' => $request->get('contactName'), 'email' => $request->get('contactEmail'), 'phone' => $request->get('contactPhone')]);
            $contract = $contract->create(['name' => $request->get('contractName'), 'date' => $request->get('date'), 'contracts_type_id' => $request->get('contracts_type_id'), 'description' => $request->get('description'), 'company_id' => $company->id]);
            $company->contacts()->attach($contact->id);
            $company->bankAccounts()->attach($request->get('bank_account_id'));
            if ($request->hasfile('image')) {
                foreach ($request->file('image') as $file) {
                    $image = new Image();
                    $nameFIle = $contract->id . round(microtime(true) * 1000) . '.' . $file->getClientOriginalExtension();
                    $image->name = $nameFIle; 
                    $image->type = $file->getClientOriginalExtension();
                    $path = $file->move(public_path('images/contracts'), $nameFIle);
                    //$path = $file->storeAs('contracts', $nameFIle);
                    $image->url = $path;
                    $image->save();
                    $contract->imagens()->attach($image->id);
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
            return Redirect::to("/companys")->withFail($e->getMessage());
        }
        return Redirect::to("/companys")->withSuccess('Registro Exitoso');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Mediators\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Mediators\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        $company = Company::with('city.department.country', 'identificationsType', 'contacts', 'bankAccounts', 'contracts.imagens')->where('id', $company->id)->first();
        $identificationsTypes = IdentificationsType::Where('enabled', 1)->get();
        $countrys = Country::Where('enabled', 1)->get();
        $departments = Department::Where('enabled', 1)->get();
        $citys = City::Where('enabled', 1)->get();
        $bankAccounts = BankAccount::Where('enabled', 1)->get();
        $contractsTypes = ContractsType::Where('enabled', 1)->get();
        // dd($company->contracts[0]->imagens);
        // dd(storage_path() . '/'. $company->contracts[0]->imagens[0]->url);
        return view('mediators.company.edit', ['company' => $company, 'departments' => $departments, 'citys' => $citys, 'identificationsTypes' => $identificationsTypes, 'countrys' => $countrys, 'bankAccounts' => $bankAccounts, 'contractsTypes' => $contractsTypes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Mediators\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        try {
            $companyObjs = Company::with('contacts', 'contracts')->where('id', $company->id)->first();
            $contact =  $companyObjs->contacts[0];
            $contract =  $companyObjs->contracts[0];
            DB::beginTransaction();
            $company->update($request->all());
            $contact->update(['name' => $request->get('contactName'), 'email' => $request->get('contactEmail'), 'phone' => $request->get('contactPhone')]);
            $contract->update(['name' => $request->get('contractName'), 'date' => $request->get('date'), 'contracts_type_id' => $request->get('contracts_type_id'), 'description' => $request->get('description')]);
            $company->bankAccounts()->sync($request->get('bank_account_id'));
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return Redirect::to("/companys")->withFail($e->getMessage());
        }
        return Redirect::to("/companys")->withSuccess('Actualizacion Exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Mediators\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        try {
            $company->enabled = ($company->enabled === 1 ? 2 : 1);
            $company->update();
        } catch (\Exception $e) {
            return Redirect::to("/companys")->withFail($e->getMessage());
        }
        return Redirect::to("/companys")->withSuccess('Empresa Actualizada');
    }
}
