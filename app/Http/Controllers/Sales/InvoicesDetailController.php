<?php

namespace App\Http\Controllers\Sales;

use App\Http\Controllers\Controller;
use App\Models\Sales\InvoicesDetail;
use Illuminate\Http\Request;

class InvoicesDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Sales\InvoicesDetail  $invoicesDetail
     * @return \Illuminate\Http\Response
     */
    public function show(InvoicesDetail $invoicesDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Sales\InvoicesDetail  $invoicesDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(InvoicesDetail $invoicesDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Sales\InvoicesDetail  $invoicesDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvoicesDetail $invoicesDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Sales\InvoicesDetail  $invoicesDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvoicesDetail $invoicesDetail)
    {
        //
    }
}
